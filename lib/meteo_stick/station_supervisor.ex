defmodule MeteoStick.StationSupervisor do
  use DynamicSupervisor
  require Logger

  alias MeteoStick.WeatherStation

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_station(data) do
    Logger.debug("Starting station: #{inspect(data)}")
    spec = {WeatherStation, data}
    DynamicSupervisor.start_child(__MODULE__, spec)
  end
end
