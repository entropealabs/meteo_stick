defmodule MeteoStick.EventManager do
  @registry_key :handlers

  def add_handler() do
    Registry.register(MeteoStick.Registry, @registry_key, [])
  end

  def publish(data) do
    Registry.dispatch(MeteoStick.Registry, @registry_key, fn entries ->
      for {pid, _} <- entries, do: send(pid, data)
    end)
  end
end
